


public class Hero {
	// Initialisation des variables
	
	private String name;
	private Integer life;
	private Integer MaxLife;
	private Integer Stamina;
	private Integer MaxStamina;
	
	public Hero(String name) {
		this.name = name;
		this.life = 100;
		this.MaxLife = 100;
		this.Stamina = 50;
		this.MaxStamina = 50;
	}
	
	public Hero() {
		 this.name = "Gregoominator";
	}
	
	
	
	protected boolean isAlive() {
		if (this.life > 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public String toString() {
		String status = (isAlive()) ? "ALIVE" : "DEAD";
		return ("[Hero] \t" + this.name + "\t Life : " + this.life + "\t Stamina : " + this.Stamina + "\t" + status);	
	}
	public void PrintStats() {
		System.out.println(toString());
	}
	// Getter
	public String getname() {
		return name;
	}
	public Integer getlife() {
		return life;
	}
	public Integer getMaxLife() {
		return MaxLife;
	}
	public Integer getStamina() {
		return Stamina;
	}
	public Integer getMaxStamina() {
		return MaxStamina;
	}
	
	// Setter
	public String setname() {
		return name;
	}
	public Integer setlife() {
		return life;
	}
	public Integer setMaxLife() {
		return MaxLife;
	}
	public Integer setStamina() {
		return Stamina;
	}
	public Integer setMaxStamina() {
		return MaxStamina;
	}
	
	
	
	
	
	
}
