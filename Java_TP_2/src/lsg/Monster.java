package lsg;

public class Monster {
	// Initialisation des variables
	
		private String MonsterName;
		private Integer life;
		private Integer MaxLife;
		private Integer Stamina;
		private Integer MaxStamina;
		public static Integer instance_count = 1;
		
		public Monster(String MonsterName) {
			this.MonsterName = MonsterName;
			this.life = 10;
			this.MaxLife = 100;
			this.Stamina = 10;
			this.MaxStamina = 50;
			PrintStats();
		}
		
		public Monster() {
			this.MonsterName = "Monster" + "_" + instance_count++;
			this.life = 10;
			this.MaxLife = 100;
			this.Stamina = 10;
			this.MaxStamina = 50;
			PrintStats();
		}
		
		protected boolean isAlive() {
			if (this.life > 0) {
				return true;
			}
			else {
				return false;
			}
		}
		
		public String toString() {
			String status = (isAlive()) ? "ALIVE" : "DEAD";
			return ("[Monster] \t" + this.MonsterName + "\t Life : " + this.life + "\t Stamina : " + this.Stamina + "\t" + status);
		}
		
		public void PrintStats() {
			
			System.out.println(toString());
		}
		
		// Getter
		public String getname() {
			return MonsterName;
		}
		public Integer getlife() {
			return life;
		}
		public Integer getMaxLife() {
			return MaxLife;
		}
		public Integer getStamina() {
			return Stamina;
		}
		public Integer getMaxStamina() {
			return MaxStamina;
		}
		
		// Setter
		public String setname() {
			return MonsterName;
		}
		public Integer setlife() {
			return life;
		}
		public Integer setMaxLife() {
			return MaxLife;
		}
		public Integer setStamina() {
			return Stamina;
		}
		public Integer setMaxStamina() {
			return MaxStamina;
		}
}
